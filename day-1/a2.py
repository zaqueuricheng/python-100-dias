# Tipos de variaveis
# Variavel é um nome para determinado valor
# Uma variavel pode ser modificada no decorrer do script

idade = 2
peso = 2.0
nome = "Zaqueu"

print(type(idade))
print(type(peso))
print(type(nome))