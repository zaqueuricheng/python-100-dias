# https://www.alura.com.br/artigos/trabalhando-com-o-dicionario-no-python

# dicionarios
# listas | tuplas ... | objetos
# array

user = {
    "nome": "Zaqueu",
    "idade": "29",
    "cidade": "Osasco",
    "telefone": "981998011"
}

print("************** Dicionario ******************")
print(user)
print(type(user))

# dicionario e lista de tuplas

users = [('Zaqueu', '29'), ('Ricardo', '48'), ('Marta', '25')]

print("************ Lista_tuplas ******************")
print(users)

print("**** Convert lista_tuplas to dict **********")
users_list = dict(users)
print(users_list)

print("***** Acessando itens de um dicionario *****")
print(users_list['Marta'])