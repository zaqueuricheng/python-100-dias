# Syntax
## Variables

x = 5 # This is comment 
y = "Hello, World!"

"""
This is multiline string comment
in python language 
"""

print(x)
print(y)