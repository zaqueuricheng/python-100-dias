# Programação funcional
#   Uma função retornar uma outra função
#   Passar uma função como parametro de uma outra função
#   "Lambeda"

def soma(a, b):
    return a + b

somar = soma
print(somar(3, 4))

def operacao_aritmetica(fn, op1, op2):
    return fn(op1, op2)

resultado = operacao_aritmetica(soma, 13, 2)
print(resultado)