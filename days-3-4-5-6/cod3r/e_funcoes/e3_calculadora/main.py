from teste import e3_calculadora_basica

n1 = float(input('Digite n1: '))
n2 = float(input('Digite n2: '))

op = input('Digite a operação: ')

if(op == '+'):    
    e3_calculadora_basica.soma(n1, n2)
elif (op == '-'):
    e3_calculadora_basica.subtracao(n1, n2)
elif (op == '*'):
    e3_calculadora_basica.multiplicacao(n1, n2)
elif (op == '/'):
    e3_calculadora_basica.divisao(n1, n2)
elif (op == '+*'):
    resultado = e3_calculadora_basica.soma_mult(n1, n2)
    print(f'O resultado final eh: {resultado}')
else:
    print('Operação inválida!')