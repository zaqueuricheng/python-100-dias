def soma(n1, n2):
    print(f'A Soma de {n1} e {n2} eh: {n1 + n2}')

def subtracao(n1, n2):
    print(f'A Subtração de {n1} e {n2} eh: {n1 - n2}')

def multiplicacao(n1, n2):
    print(f'A Multiplicacao de {n1} e {n2} eh: {n1 * n2}')

def divisao(n1, n2):
    print(f'A divisao de {n1} e {n2} eh: {n1 / n2}')

# RETORN
def soma_mult(n1, n2, n3 = 2):
    return n1 + n2 * n3