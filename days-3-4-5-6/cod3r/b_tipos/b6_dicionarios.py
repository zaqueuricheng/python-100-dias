# Dicionarios (dict)
# Usa o mesmo simbolo {} para dif set/conjuntos

# Dicionario eh "chave":"valor" 
# No lugar de indexação numerica ele usa chave para se referir a um determinado elemento

aluno = {
    'nome': 'Zaqueu Ricardo',
    'nota': 9.2,
    'ativo': True
}

print(type(aluno))

# ACESSANDO VALOR ATRAVÉS DA CHAVE
print(aluno['nome'])
print(aluno['nota'])
print(aluno['ativo'])

# TAMANHO DO DICT
print(len(aluno))