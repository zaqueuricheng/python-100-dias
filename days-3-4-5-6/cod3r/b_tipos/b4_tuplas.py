# Aceita valores repetidos
# Algumas funcionalidade são disponivel para as listas também

nomes = ('Ana', 'Bia', 'Gui', 'Ana')

print(type(nomes))
print(nomes)

print('bia' in nomes) # Verifica se bia esta presente na tupla (Saida será FALSE ou TRUE)

# IMPRIME O 1º ELEMENTO
print(f'1º Elemento eh: {nomes[0]}')

# IMPRIME O 1º e 2º ELEMENTO
print(f'1º e 2º Elemento eh: {nomes[0:2]}')

# IMPRIME O 2º e o ULTIMO ELEMENTO
print(f'2º e o ULTIMO elemento eh: {nomes[1:-1]}')

# IMPRIME TODOS OS ELEMENTOS A PARTIR DO 2º
print(f'Todos os elementos a partir do 2º: {nomes[1:]}')