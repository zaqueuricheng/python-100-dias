a = 3 # Inteiro
b = 4.4 # Float

print(a + b)

texto = "Sua idade eh... " # String
idade = 23 # Inteiro

#print(texto + str(idade)) # Faz a conversão de inteiro para string, sem o str o codigo da erro

print(f'{texto} {idade}') # Melhor forma de imprimir dois tipos da dados diferentes

print(3 * 'Bom dia | ') # Imprime três vezes a string "Bom dia"

# Em python não existe CONSTANTE, existe uma convensão de uso de letras maisculas para def. CONST 

PI = 3.14
raio = float(input("Informe o raio da circu? "))

print(type(PI))
print(type(raio))

print("A area da circ. eh: ", (PI*raio*raio), "m2")

area = PI*pow(raio, 2)
print(f'A area da circ. eh: {area} m2') # {} = interpolar