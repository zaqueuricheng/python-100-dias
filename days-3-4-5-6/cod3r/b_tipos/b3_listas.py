# Lista aceita valores repetidos

nums = [1, 2, 3]

print(type(nums))

print(f'Tamanho inicial: {len(nums)}')

# Adicionar um novo elemento na lista
nums.append(3)
print(f'Tamanho final: {len(nums)}')

# Usando os indices da lista
nums[3] = 100
print(nums)

# Comando insert
nums.insert(0, -200) # Inserir no indice 0 da lista o elemento -200
print(nums)