#!python3

# ******************************* A3 - PACOTES
'''
from pacote.sub import hello # 1
from pacote.sub import arquivo # 2
'''

# ******************************* B3 - TIPOS DE DADOS
# import b_tipos.b1_variaveis
# from b_tipos import b2_basicos
# from b_tipos import b3_listas
# from b_tipos import b4_tuplas
# from b_tipos import b5_conjuntos
# from b_tipos import b6_dicionarios

# ****************************** C3 - OPERADORES
                                    # 1 - Unários: operam em cima de um unico operando | not
                                    # 2 - Binários: 
                                    # 3 - Ternários:
# from c_operadores import c1_unarios
# import c_operadores.c2_aritmeticos
# from c_operadores import c3_relacionais
# import c_operadores.c4_atribuicao
# import c_operadores.c5_logicos
# import c_operadores.c6_ternarios

# ***************************** D4 - CONTROLE
# import d_controle.d1_if
# import d_controle.d2_if
# from d_controle import d3_for
# from d_controle import d4_while

# **************************** D6 - FUNÇÕES

# from e_funcoes import e1_basic ### basic is the modulo
# from e_funcoes.basic import saudacoes ### Outra forma de chamar a função
                                        ### 
# e1_basic.saudacoes() # invoca a função saudações que esta dentro do modulo (arquivo) basic

# from e_funcoes import e2_parameters
# e2_parameters.names('Zaqueu')

# from e_funcoes import e4_def_param_padrao

# e4_def_param_padrao.saudacao('Zaqueu')

'''
from e_funcoes import e5_args

s = e5_args.soma(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
print(s)

resultado = e5_args.resultado_final(nome='Zaqueu', nota=6.5)
print(resultado)
'''

from e_funcoes import e6_funcional
