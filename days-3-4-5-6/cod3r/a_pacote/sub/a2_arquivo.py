modulo = __name__
pacote = __package__

print("O modulo do arquivo eh: ", modulo) # __name__ é uma variável que mostra o caminho (modulo) completo do arquivo
print("O pacote do arquivo eh: ", pacote) # __package é uma variavel que mostra o caminho (pacote) do arquivo

# Esses caminhos podem ser usados na "main" para chamar um determinado "modulo", "pacote" ou "arquivo"
