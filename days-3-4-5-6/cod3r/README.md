CURSO RÁPIDO DE PYTHON: https://www.youtube.com/watch?v=oUrBHiT-lzo

Shebang(#): https://pt.stackoverflow.com/questions/190883/o-que-%C3%A9-shebang
Shebang(#): Indica o interpretador que deve ser usado para executar um determinado programa/ficheiro

Exemplos shebangs:
    #!/usr/bin/env python
    #!/bin/bash

Runtimes -> python3 | python2

plugins -> python | code runner

Comandos Linux
    chmod a+x script.sh
    echo '#!/bin/rm' > ex.txt && chmod 777 ex.txt