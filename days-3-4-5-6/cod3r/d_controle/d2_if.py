a = 'valor' # Qualquer valor diferente de 0 o if avalia como verdadeiro

a = 0
a = '' # Considerado como falso tb, se tiver espaço sera verdadeiro
a = []
a = {}
a = -1

if a:
    print('Existe!!!')
else:
    print('Não existe ou zero ou vazio!')