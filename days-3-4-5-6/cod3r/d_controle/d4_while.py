
op = ' '

def soma(n1, n2):
    print("A soma eh: ", (n1+n2))

def sub(n1, n2):
    print("A subtracao eh: ", (n1-n2))

def mult(n1, n2):
    print("A multipicação eh: ", (n1*n2))

def div(n1, n2):
    print("A divisão eh: ", (n1/n2))

def mod(n1, n2):
    print("O resto da div eh: ", (n1%n2))
    
    if((n1%n2) == 0):
        print('Numero par')
    else:
        print('Numero impar')

while op != '-1': # ********************************** LAÇO WHILE
    op = input('Digite a operação ou (-1) para sair: ')
    
    if(op == '-1'):
        print('Fechando!!!')
    else:
        n1 = float(input('Digite n1: '))
        n2 = float(input('Digite n2: '))

        if(op == '+'):
            print(soma(n1, n2))
        elif(op == '-'):
            print(sub(n1, n2))
        elif(op == '*'):
            print(mult(n1, n2))
        elif(op == '/'):
            print(div(n1, n2))
        elif(op == '%'):
            print(mod(n1, n2))
        else:
            print('Operação inválida!')