nota = float(input('Informe a nota do aluno: '))
comportado = True if input('Comportado (s/n): ') == 'y' else False

if nota >= 9 and comportado: # Entra no quadro de honra se for comportado
    print('Duas palavras: para bens! :P') # Bloco de código
    print('Quadro de honra!')
elif nota >= 7:
    print("Aprovado!")
elif nota >= 5.5:
    print('Recupareção!')
elif nota >= 4:
    print('Recuperação + Trabalho')
else:
    print("Reprovado!")

print(f'Nota: {nota}')