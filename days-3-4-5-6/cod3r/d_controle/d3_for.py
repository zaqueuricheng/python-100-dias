#!pythonm3

print('Repetição 1: ')
for i in range(10): # O range vai te dar um valor de 0 até 9
    print(i)

print('Repetição 2: ')
for i in range(1, 11): # Difinindo o inicio e fim dentro do range
    print(i)

print('Repetição 3: ')
for i in range(1, 100, 7): # range(inicio, fim, passo)
    print(i)

print('Repetição 4: ')
for i in range(20, 0, -3): # Decrementa
    print(i)

print('Percorrendo a lista: ')
nums = [2, 4, 6, 8]
for n in nums:
    print(n)

print('Mostrando todos elementos na mesma linha: ')
lista = [1, 3, 5, 7]
for m in lista:
    print(m, end=' ') # Para mostrar na mesma linha

print('Percorrendo uma string: ')
texto = 'Python eh muito massa!'
for letra in texto:
    print(letra, end='')

# Percorrendo um dicionário
print('Percorrendo um dicionario: ')

produto = {
    'nome': 'Caneta',
    'preco': 8.80,
    'desconto': 0.5
}

for k in produto: # k eh o atributo
    print(k, '==>', produto[k])

