# Variaveis em python não tem um tipo fixo...
# Você pode começar com um valor e depois mudar ao longo do programa
# São operadores binarios

resultado = 2 # conteúdo
print(resultado)

resultado = 'texto' # conteúdo modificado
print(resultado)

# atribuição aditiva
resultado = 3
resultado += 2 # resultado = resultado + 2
print(resultado)

# atribuição subtrativa
resultado -= 5
print(resultado)

# atribuição multiplicativa
resultado *= 5

# atribuição divisao
resultado /= 5

# atribuição modular
resultado %= 5