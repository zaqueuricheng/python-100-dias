# Operadores unários operam em cima de um único operando

print(not False) # Invert o valor
print(not True)

y = 4

print(-3) # Operador "-" operando em cima de um único operando
print(-y)
#print(--y) # Inverte o valor para positivo
print(-+y)