# +3 prefix
# x++ postfix
# x + y infix

x = 10
y = 3

print(x + y)
print(x - y)
print(x * y)
print(x / y)
print(x % y) # Modulo resto da divisão. Exemplo: (par % 2) == 0 | (impar % 2) == 1