B1 - INPUT
B2 - FUNÇÕES
B3 - CALCULADORA BÁSICA
B4 - MELHORIA DA CALCULADORA: if | elif | else

********************** Revisão
A0 - IMPRESSÃO
print("Hello World")

A1 - VARIÁVEIS
n1 = 2
n2 = 5
nome = ("Zaqueu")

A2 - TYPE
type(1)
type(1.0)
type("string")
type(True) # booleano
type(4+3j) # complex

A3 - LISTAS
frutas = ["banana", "morango", "uva"]

A4 - LAÇOS
for n in m:
    print()

A5 - DECISÃO
if ():
    pass
elif ():
    pass
else
    pass

A6 - TUPLAS
tuplas = ("nome", "peso", "altura", "idade", 10)

A7 - DICIONÁRIO
users = {
    "nome": "Zaqueu",
    "idade": "29",
    "peso": "60",
    "altura": "170"
}

A8 - FUNÇÕES
def soma():
    soma = (n1 + n2)