# INPUT
# observa q o int() converte o valor de entrada para inteiro
# se não for usado o int, será considerado como string de forma padrão

# "float" "bool" "complex" "string"

n1 = int(input("Digite n1: ")) 
n2 = int(input("Digite n2: "))

soma = n1 + n2

print(soma)