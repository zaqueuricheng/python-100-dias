# Calculadora básica
# Funções e passagem de parâmetros
# Inputs

n1 = float(input("Digite n1: "))
n2 = float(input("Digite n2: "))

def soma(n1, n2):
    print(n1 + n2)

print("soma")
soma(n1, n2)

def subtracao(n1, n2):
    print(n1 - n2)

print("subtracao")
subtracao(n1, n2)

def multiplicacao(n1, n2):
    print(n1 * n2)

print("multiplicacao")
multiplicacao(n1, n2)

def divisao(n1, n2):
    print(n1 / n2)

print("divisao")
divisao(n1, n2)